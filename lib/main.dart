import 'package:flutter/material.dart';
import 'package:flutter_app/ui/home_page.dart';
import 'package:flutter_app/ui/koikoi_page.dart';
import 'package:flutter_app/ui/hachihachi_page.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

final logger = Logger(
  printer: PrettyPrinter(
    methodCount: 0,
    errorMethodCount: 5,
    lineLength: 50,
    colors: true,
    printEmojis: true,
    printTime: false,
  ),
);

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '花札点数計算',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: HomePage(title: '花札点数計算'),
      initialRoute: '/home',
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => HomePage(title: '花札点数計算'),
        '/koikoi': (BuildContext context) => KoikoiPage(title: 'こいこい'),
        '/hachihachi': (BuildContext context) => HachihachiPage(title: 'はちはち'),
      },
    );
  }
}
